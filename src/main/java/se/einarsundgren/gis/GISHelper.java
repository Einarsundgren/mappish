/*
 * Copyright 2015 Einar Sundgren
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package se.einarsundgren.gis;

import org.osmdroid.util.BoundingBoxE6;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;

public class GISHelper {
	/***
	 * Translates a radius in real world meters to the corresponding pixel value for
	 * the given map projection and latitude. Projection needed to adjust for appropriate zoom level 
	 * and latitude needed for a proper geodesi calculation since proportions would change 
	 * depending och latitude and the built in function is for use over the equator. Unchanged it will give a 
	 * significant error if used in the northern hemisphere.
	 * @param meters The radius in real world meters.
	 * @param map The OSMDroid map projection to get the circle drawn
	 * @param latitude latitude of the circle. For Stalker app the centerpoint of the circle is used.
	 * @return an integer with the corresponding number of pixels for the latitude and projection.
	 */
	public static int metersToRadius(float meters, MapView map, double latitude) {
		return (int) (map.getProjection().metersToEquatorPixels(meters) * (1 / Math
				.cos(Math.toRadians(latitude))));
	}
	
	/**
	 * Implementation of the Haversin formula for calculating the distance
	 * between two points on a sphere.
	 * 
	 * @param startLat starting latitude
	 * @param startLon staring longitude
	 * @param endLat end latitude 
	 * @param endLon end longitude
	 * @return distance in km.
	 */
	public static Double haversin(Double startLat, Double startLon,
			Double endLat, Double endLon) {

		final int r = 6371; // Radius of the earth i km. Decides what unit the return is in. (km * 1000 = meters) 
		/*
		 * startLat = 57.69208512; startLon = 11.92348234; endLat = 57.688991;
		 * endLon = 11.920123;
		 * 
		 * Debug distances should give 0.3978 km.
		 */

		Double latDistance = toRad(endLat - startLat);
		Double lonDistance = toRad(endLon - startLon);
		Double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
				+ Math.cos(toRad(startLat)) * Math.cos(toRad(endLat))
				* Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
		Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		//Double distance = r * c;
		
		//Return distance between two points in km
		return (double)r * c;
	}
	/**
	 * Takes a geopint as startposition and calculates another geopint depending on 
	 * from the x,y offset in meters to that geopoint.
	 * @param startPoint The gps position you are originating from
	 * @param xOff X offset from that point in meters
	 * @param yOff Y offset from that point in meters
	 * @return geoPoint The gps point at the given offset
	 */
	public static GeoPoint getCoordsAtOffset(GeoPoint startPoint, double xOff, double yOff){
		 //Position, decimal degrees
		 double lat = startPoint.getLatitude();
		 double lon = startPoint.getLongitude();

		 //Earth’s radius, sphere
		 //final int r=6378137;
		 final int r=6372814;

		 //Coordinate offsets in radians
		 double dLat = yOff/r; 
		 double dLon = xOff/(r*Math.cos(Math.PI*lat/180));

		 //OffsetPosition, decimal degrees
		 double latO = lat + dLat * 180/Math.PI;
		 double lonO = lon + dLon * 180/Math.PI; 

		return new GeoPoint(latO, lonO);
	}
/**
 * Changes the input value in degrees to radians
 * @param value The value in degrees
 * @return the input value in radians.
 */
	private static double toRad(double value) {
		return value * Math.PI / 180;
	}
	
	/**
	 * Takes a bounding box from OSM droid and calculates its distanse 
	 * east to west in meters 
	 * @param box The coordinates for a box determining the area of interest
	 * @return The distance in meters
	 */
	public static int eastToWestMeters(BoundingBoxE6 box){
    int meters = (int)Math.round(GISHelper.haversin(
			(double)box.getLatNorthE6()/1000000, 
			(double)box.getLonWestE6()/1000000, 
			(double)box.getLatNorthE6()/1000000, 
			(double)box.getLonEastE6()/1000000)*1000); 
	return meters;
	}
	
	
	/**
	 * Takes a bounding box from OSM droid and calculates its distanse 
	 * north to south in meters 
	 * @param box The coordinates for a box determining the area of interest
	 * @return the distance in meters
	 */
	public static int nortToSouthMeters(BoundingBoxE6 box){
    int meters = (int)Math.round(GISHelper.haversin(
			(double)box.getLatNorthE6()/1000000, 
			(double)box.getLonWestE6()/1000000, 
			(double)box.getLatSouthE6()/1000000, 
			(double)box.getLonWestE6()/1000000)*1000); 
    return meters;
	}
	

	
	
}

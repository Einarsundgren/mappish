
/*
 * Copyright 2015 Einar Sundgren
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package se.einarsundgren.mappish;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.LocationManager;
import android.preference.PreferenceManager;
import android.util.Log;

import java.util.Calendar;

public class MappishApplication extends Application {

	private final String TAG = "MappishApplication";
    public static final String MAPPISH_PREFERENCES = "mappish_preferences";
	// Prefs keys
	public static final String GRID_SIZE ="gridSize";
	public static final String MARKER_TYPE = "markerType";
	public static final String MAP_COLORING_TYPE = "mapColoringType";
    public static final String MAP_TYPE = "mapType";
	public static final String BLUR_TYPE ="blurType";
	public static final String ZOOM_LEVEL = "zoomLevel";
	public static final String MAX_ZOOM_LEVEL = "maxZoomLevel";
	public static final String MIN_ZOOM_LEVEL = "minZoomLevel";
	public static final String PREF_RECORD_POS = "pref_record_pos";
	public static final String QUERY_START_TIME = "query_start_time";
	public static final String QUERY_END_TIME = "query_end_time";
	public static final String START_DATE_TOGGLE = "start_date_toggle";
    public static final String END_DATE_TOGGLE = "end_date_toggle";
    public static final String START_TIME_TOGGLE = "start_time_toggle";
    public static final String END_TIME_TOGGLE = "end_time_toggle";

	
	// Prefs values
	public static final int VIEW_HEATMAP = 0;
	public static final int VIEW_COLOR_SHADES = 1;

    public static final int VIEW_PLACES = 0;
    public static final int VIEW_PATHS_AND_PLACES = 1;
	
	public static final int MARKER_CIRCLE = 0;
	public static final int MARKER_RECT = 1;
	public static final int MARKER_HEXAGON = 2;
	
	public static final int BLUR_GAUSSIAN = 0;
	public static final int BLUR_BOX = 1;
	
	public static final int MIN_GRID_SIZE = 10;
	//private MappishDB mappishDB;
	//private MappishDBHelper mappishDBHelper;
	private LocationManager locationManager;
	private Intent UpdaterServiceIntent;
	private boolean gpsProbsNotified = false;
	public static int sleepTime = 60000;

    public static final int GPS_NOTIFICATION_ID = 1;
    private SharedPreferences preferences;
    public String appName = "MappishAppName";
	
	@Override
	public void onCreate() {
		super.onCreate();
		//Log.d(TAG,"Application loaded"
        preferences = getSharedPreferences(MappishApplication.MAPPISH_PREFERENCES, Context.MODE_PRIVATE);
        appName = "Mappish App started";
	}

    public void stopUpdates(){
        Intent coordServiceIntent = new Intent(this, GetCoordService.class);
        PendingIntent pendingCoordServiceIntent = PendingIntent.getService(this, 0, coordServiceIntent, 0);
        AlarmManager alarmManager = (AlarmManager)this.getSystemService(Application.ALARM_SERVICE);
        alarmManager.cancel(pendingCoordServiceIntent);
        preferences.edit().putBoolean(MappishApplication.PREF_RECORD_POS, false).apply();

        Log.d(TAG,"Stoping updates");
    }

    public void startUpdates(){
        AlarmManager alarmManager = (AlarmManager)getSystemService(ALARM_SERVICE);
        Intent coordServiceIntent = new Intent(this, GetCoordService.class);
        PendingIntent pendingCoordServiceIntent = PendingIntent.getService(this, 0, coordServiceIntent, 0);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.add(Calendar.SECOND, 10);
        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), MappishApplication.getSleepTime(), pendingCoordServiceIntent);
        preferences.edit().putBoolean(MappishApplication.PREF_RECORD_POS, true).apply();
        Log.d(TAG,"Starting updates");
    }

/*
	public MappishDBHelper getMappishDBHelper() {
		return mappishDBHelper;
	}

	public void setMappishDBHelper(MappishDBHelper mappishDBHelper) {
		this.mappishDBHelper = mappishDBHelper;
	}

	public MappishDB getMappishDB() {
		return mappishDB;
	}

	public void setMappishDB(MappishDB mappishDB) {
		this.mappishDB = mappishDB;
	}
	*/

	public LocationManager getLocationManager() {
		return locationManager;
	}

	public void setLocationManager(LocationManager locationManager) {
		this.locationManager = locationManager;
	}

	public static int getSleepTime() {
		return sleepTime;
	}

	public static void setSleepTime(int st) {
		sleepTime = st;
	}


	public Intent getUpdaterServiceIntent() {
		return UpdaterServiceIntent;
	}


	public void setUpdaterServiceIntent(Intent updaterServiceIntent) {
		UpdaterServiceIntent = updaterServiceIntent;
	}


	public boolean isGpsProbsNotified() {
		return gpsProbsNotified;
	}


	public void setGpsProbsNotified(boolean gpsProbsNotified) {
		this.gpsProbsNotified = gpsProbsNotified;
	}

}

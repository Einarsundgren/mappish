
/*
 * Copyright 2015 Einar Sundgren
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package se.einarsundgren.mappish;

import java.util.Calendar;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.location.LocationManager;
import android.os.IBinder;
import android.util.Log;


public class UpdaterService extends Service {
	//private UpdaterThread updaterThread;
	private MappishApplication app;
	private LocationManager locationManager;
	private AlarmManager alarmManager;
	private PendingIntent pendingCoordIntent;
	String TAG = "UpdaterService";
	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		//Log.d(TAG,"Inside service Started onStartCommanad HERE");
		//Toast.makeText(this, "Updater service started", Toast.LENGTH_SHORT).show();
	return startId;
	}
	
	@Override
	public void onCreate(){
		  alarmManager = (AlarmManager)getSystemService(ALARM_SERVICE);
		  Intent coordServiceIntent = new Intent(this, GetCoordService.class);
		  pendingCoordIntent = PendingIntent.getService(this, 0, coordServiceIntent, 0);
		  Calendar calendar = Calendar.getInstance();
          calendar.setTimeInMillis(System.currentTimeMillis());
          calendar.add(Calendar.SECOND, 10);
          
		  alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), MappishApplication.getSleepTime(), pendingCoordIntent);
		//Log.d(TAG,"Inside service Started oncreate by timer...");
	}
	@Override
	public void onDestroy(){
		//Toast.makeText(this, "Updater service closed", Toast.LENGTH_SHORT).show();
		alarmManager.cancel(pendingCoordIntent);
	}
}


/*
 * Copyright 2015 Einar Sundgren
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package se.einarsundgren.mappish.fragment;


import se.einarsundgren.mappish.MappishApplication;
import se.einarsundgren.mappish.R;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;

public class PreferencesFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener{
	
	private MappishApplication app;
	
	 @Override
	 public void onCreate(Bundle savedInstanceState) {
	  super.onCreate(savedInstanceState);
	  	  // Load the preferences from an XML resource
	        addPreferencesFromResource(R.xml.prefs);
	        app = (MappishApplication)getActivity().getApplication();
	        PreferenceManager.getDefaultSharedPreferences(getActivity()).registerOnSharedPreferenceChangeListener(this);
	 }

	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences,
			String key) {
		
		if (key.equals(MappishApplication.PREF_RECORD_POS)){

			// If key is false and thread is running. Send interrupt signal.
			// When thread catches it execution stops.
			if (!sharedPreferences.getBoolean(key, true) ){
				System.out.println("Should be off");
				//app.stopService(updaterServiceIntent);
                app.stopUpdates();


            } else if (sharedPreferences.getBoolean(key, true) ){
				System.out.println("Should be on ");
				//app.startService(updaterServiceIntent);
                app.startUpdates();
			} 
		}	
	}

}

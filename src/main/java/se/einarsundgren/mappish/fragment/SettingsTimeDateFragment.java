
/*
 * Copyright 2015 Einar Sundgren
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package se.einarsundgren.mappish.fragment;

import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import se.einarsundgren.mappish.MappishApplication;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Fragment;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.ToggleButton;
import se.einarsundgren.mappish.R;


public class SettingsTimeDateFragment extends Fragment{
	private OnMapControlChange onMapControlChange; //TODO: Try to remove this. Seems redundant.
	private SharedPreferences preferences;
	private Activity parentActivity;

	private int startYear;
	private int startMonth;
	private int startDay;
	private int startHour;
	private int startMinute;

	private int endYear;
	private int endMonth;
	private int endDay;
	private int endHour;
	private int endMinute;
    private final Calendar c = Calendar.getInstance();
	
//TODO: Clean up how time is showing.
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
		
		parentActivity = getActivity();
		View rootView = inflater.inflate(R.layout.settings_datetime_fragment,
				container, false);
		
		
		parentActivity = getActivity();
		
		preferences = parentActivity.getSharedPreferences(MappishApplication.MAPPISH_PREFERENCES, Context.MODE_PRIVATE);

        rootView.setBackgroundColor(Color.WHITE);

		final ToggleButton startDateToggle = (ToggleButton) rootView.findViewById(R.id.setStartDate);
		final ToggleButton endDateToggle = (ToggleButton) rootView.findViewById(R.id.setEndDate);
		
		final ToggleButton startTimeToggle = (ToggleButton) rootView.findViewById(R.id.setStartTime);
		final ToggleButton endTimeToggle = (ToggleButton) rootView.findViewById(R.id.setEndTime);

		final TextView fromTime = (TextView) rootView.findViewById(R.id.searchIntervalStart);
		final TextView toTime = (TextView) rootView.findViewById(R.id.searchIntervalEnd);


//        Calendar c = Calendar.getInstance();
        final int thisYear = c.get(Calendar.YEAR); // Just search a year into the future
        final int thisMonth = c.get(Calendar.MONTH);
        final int thisDay = c.get(Calendar.DAY_OF_MONTH);
        final int thisHour = c.get(Calendar.HOUR_OF_DAY);
        final int thisMinute = c.get(Calendar.MINUTE);

        /*
        *Initialize the elements with what is saved in the prefs.
         */
        if (preferences.getBoolean(MappishApplication.START_DATE_TOGGLE,false)){
            startDateToggle.setChecked(true);

            if (preferences.getBoolean(MappishApplication.START_TIME_TOGGLE,false)){
                startTimeToggle.setChecked(true);
            } else {
                startTimeToggle.setChecked(false);
            }
        } else {
            startDateToggle.setChecked(false);
            startTimeToggle.setChecked(false);
            startTimeToggle.setEnabled(false);
            int timeStamp = componentTimeToTimestamp(1970, 0, 0);
            preferences.edit().putInt(MappishApplication.QUERY_START_TIME, timeStamp).apply();
        }


        if (preferences.getBoolean(MappishApplication.END_DATE_TOGGLE,false)){
            endDateToggle.setChecked(true);
            if (preferences.getBoolean(MappishApplication.END_TIME_TOGGLE,false)){
                endTimeToggle.setChecked(true);
            } else {
                endTimeToggle.setChecked(false);
            }
        } else {
            endDateToggle.setChecked(false);
            endTimeToggle.setChecked(false);
            endTimeToggle.setEnabled(false);
            int timeStamp = componentTimeToTimestamp(thisYear+1, thisMonth, thisDay);
            preferences.edit().putInt(MappishApplication.QUERY_END_TIME, timeStamp).apply();
        }
        setTimespanText(fromTime, toTime);




        OnClickListener viewButtonListener = new OnClickListener() {

            /*
            These two listeners fires whenever the date picker is set to a date.
            */
			OnDateSetListener onDateSetStartListener = new OnDateSetListener() {		
				@Override
				public void onDateSet(DatePicker view, int year, int monthOfYear,
						int dayOfMonth) {

					startDay = dayOfMonth;
					startYear = year;
					startMonth = monthOfYear;
					
					int timeStamp = componentTimeToTimestamp(startYear, startMonth, startDay);
					preferences.edit().putInt(MappishApplication.QUERY_START_TIME, timeStamp).apply();
                    preferences.edit().putBoolean(MappishApplication.START_DATE_TOGGLE,true).apply();
					/*
					Toast.makeText(parentActivity,
							"StartTime Turning On: " + timeStamp,
							Toast.LENGTH_SHORT).show();*/
                    setTimespanText(fromTime, toTime);

				}
			};
			
			OnDateSetListener onDateSetEndListener = new OnDateSetListener() {
				@Override
				public void onDateSet(DatePicker view, int year, int monthOfYear,
						int dayOfMonth) {
                    endYear = year;
                    endMonth = monthOfYear;
                    endDay = dayOfMonth;
					int timeStamp = componentTimeToTimestamp(endYear, endMonth, endDay);
					preferences.edit().putInt(MappishApplication.QUERY_END_TIME, timeStamp).apply();
                    preferences.edit().putBoolean(MappishApplication.END_DATE_TOGGLE,true).apply();
                    setTimespanText(fromTime, toTime);
				}
			};
			/*
			*These two listeners fires whenever the time for start or end is set.
			 */
			OnTimeSetListener onTimeSetStartListener = new OnTimeSetListener() {
				@Override
				public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
					startHour = hourOfDay;
					startMinute = minute;
					int hourMin = componentTimeToTimestamp(hourOfDay, minute);
                    //Get current Year, month and day
                    int yearMonthDay = preferences.getInt(MappishApplication.QUERY_START_TIME,0);
					preferences.edit().putInt(MappishApplication.QUERY_START_TIME, yearMonthDay+hourMin).apply();
                    preferences.edit().putBoolean(MappishApplication.START_TIME_TOGGLE,true).apply();

                    /*
                    Toast.makeText(parentActivity,
							"StartTime från OnClick: " + hourMin,
							Toast.LENGTH_SHORT).show();
					*/


                    setTimespanText(fromTime, toTime);
				}
			};
			
			OnTimeSetListener onTimeSetEndListener = new OnTimeSetListener() {
				@Override

				public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
					
					endHour = hourOfDay;
					endMinute = minute;
                    int yearMonthDay = preferences.getInt(MappishApplication.QUERY_END_TIME,0);
					int hourMin = componentTimeToTimestamp(hourOfDay, minute);
					preferences.edit().putInt(MappishApplication.QUERY_END_TIME, yearMonthDay+hourMin).apply();
                    preferences.edit().putBoolean(MappishApplication.END_TIME_TOGGLE,true).apply();

                    setTimespanText(fromTime, toTime);
				}
			};


            /*
            * This listener is to react when the time/date selector is untoggeled.
             */
			@Override
			public void onClick(View view) {
				switch (view.getId()){
				case R.id.setStartDate:
					if (startDateToggle.isChecked()){
					DatePickerDialog sdpd = new DatePickerDialog(parentActivity, onDateSetStartListener, thisYear, thisMonth, thisDay);
					sdpd.show();
                    // Switch on timelimit if this is used.
                    startTimeToggle.setEnabled(true);
					} else {
						startYear = 1970;
						startMonth = 0;
						startDay = 0;
						int timeStamp = componentTimeToTimestamp(startYear, startMonth, startDay);
						preferences.edit().putInt(MappishApplication.QUERY_START_TIME, timeStamp).apply();
                        startTimeToggle.setEnabled(false);
                        startTimeToggle.setChecked(false);
                        preferences.edit().putBoolean(MappishApplication.START_DATE_TOGGLE,false).apply();

					}
                    setTimespanText(fromTime, toTime);
					break;
					
				case R.id.setEndDate:
					if (endDateToggle.isChecked()){
					DatePickerDialog edpd = new DatePickerDialog(parentActivity, onDateSetEndListener, thisYear, thisMonth, thisDay);
					edpd.show();
                    endTimeToggle.setEnabled(true);
                    endTimeToggle.setChecked(false);
					} else {

					int timeStamp = componentTimeToTimestamp(thisYear+1, thisMonth, thisDay);
					preferences.edit().putInt(MappishApplication.QUERY_END_TIME, timeStamp).apply();
                    endTimeToggle.setEnabled(false);
                    endTimeToggle.setChecked(false);
                    preferences.edit().putBoolean(MappishApplication.END_DATE_TOGGLE,false).apply();
					}
                    setTimespanText(fromTime, toTime);
					break;
					
				case R.id.setStartTime:
					if (startTimeToggle.isChecked()){
					TimePickerDialog stpd = new TimePickerDialog(parentActivity, onTimeSetStartListener, thisHour, thisMinute, true);

                        stpd.show();
					} else {
						int hourMin = componentTimeToTimestamp(startHour, startMinute);
                        int yearMonthDay = preferences.getInt(MappishApplication.QUERY_START_TIME, 0);
						preferences.edit().putInt(MappishApplication.QUERY_START_TIME, yearMonthDay-hourMin).apply();
                        preferences.edit().putBoolean(MappishApplication.START_TIME_TOGGLE,false).apply();

					}
                    setTimespanText(fromTime, toTime);
					break;
					
				case R.id.setEndTime:
					if (endTimeToggle.isChecked()){
					TimePickerDialog etpd = new TimePickerDialog(parentActivity, onTimeSetEndListener, thisHour, thisMinute, true);
					etpd.show();
					} else {
                        // If nothing choosen. Set end time one year in the future
                        int hourMin = componentTimeToTimestamp(endHour, endMinute);
                        int yearMonthDay = preferences.getInt(MappishApplication.QUERY_END_TIME, 0);
                        preferences.edit().putInt(MappishApplication.QUERY_END_TIME, yearMonthDay-hourMin).apply();
                        preferences.edit().putBoolean(MappishApplication.END_TIME_TOGGLE,false).apply();
					}
                    setTimespanText(fromTime, toTime);
					break;
				}


                /*
                Insert it here
                 */

                setTimespanText(fromTime, toTime);
			}
			
		};


		startDateToggle.setOnClickListener(viewButtonListener);
		endDateToggle.setOnClickListener(viewButtonListener);
		startTimeToggle.setOnClickListener(viewButtonListener);
		endTimeToggle.setOnClickListener(viewButtonListener);
		
		return rootView;
	}

	@Override
	public void onResume() {
		super.onResume();

	}
	
	// Overrides on Attach and makes sure it implements the OnMapControlChange
	@Override
	public void onAttach(Activity activity){
        super.onAttach(activity);

	       try {
	    	   onMapControlChange = (OnMapControlChange) activity;
	        } catch (ClassCastException e) {
	            throw new ClassCastException(activity.toString()
	                    + " must implement OnHeadlineSelectedListener");
	        }
	}

    private void setTimespanText(TextView fromTime, TextView toTime) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm"); //TODO: Fix this to show local time variations.
        Date sd = new Date(((long) preferences.getInt(MappishApplication.QUERY_START_TIME, 0)) * 1000L);
        Date ed = new Date(((long) preferences.getInt(MappishApplication.QUERY_END_TIME, 0)) * 1000L);
        fromTime.setText("From: " + sdf.format(sd));
        toTime.setText("To: " + sdf.format(ed));
    }

	//TODO: Move to helper package
	int componentTimeToTimestamp(int hour, int minute) {
	    return hour*3600 + minute * 60;
	}

    int componentTimeToTimestamp(int year, int month, int day) {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, year);
        c.set(Calendar.MONTH, month);
        c.set(Calendar.DAY_OF_MONTH, day);
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);

        return (int) (c.getTimeInMillis() / 1000L);
    }

}

/*
 * Copyright 2015 Einar Sundgren
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package se.einarsundgren.mappish.fragment;

import se.einarsundgren.mappish.MappishApplication;
import se.einarsundgren.mappish.R;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.graphics.Color;

public class SettingsHeatmapFragment extends Fragment {

    protected MappishApplication app;
    private SharedPreferences preferences;
    // Prefs values
    int gridSize;
    int viewColoringType;
    int viewType;
    int blurType;

    OnMapControlChange onMapControlChange;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        //Experiment to see if we can avoid crashes
        setRetainInstance(true);

        if (!this.isInLayout() && container == null) return null;
        app = (MappishApplication) getActivity().getApplication();

        // Inflate the roow view.
        View rootView = inflater.inflate(R.layout.settings_heatmap_fragment,
                container, false);
        Activity parentActivity = getActivity();

        preferences = parentActivity.getSharedPreferences(MappishApplication.MAPPISH_PREFERENCES, Context.MODE_PRIVATE);

        rootView.setBackgroundColor(Color.WHITE);

        gridSize = preferences.getInt(MappishApplication.GRID_SIZE, 20);
        viewColoringType = preferences.getInt(MappishApplication.MAP_COLORING_TYPE, MappishApplication.VIEW_COLOR_SHADES);
        viewType = preferences.getInt(MappishApplication.MAP_TYPE, MappishApplication.VIEW_PLACES);
        blurType = preferences.getInt(MappishApplication.BLUR_TYPE, MappishApplication.BLUR_GAUSSIAN);

        // Too small gridsize is not allowed.
        if (gridSize < 10) gridSize = 10;

        SeekBar seekBarGridSize = (SeekBar) rootView.findViewById(R.id.seekBar_gridsize);
        RadioButton radioButtonHeatMap = (RadioButton) rootView.findViewById(R.id.button_view_heatmap);
        RadioButton radioButtonColorShade = (RadioButton) rootView.findViewById(R.id.button_view_colorshade);
        RadioButton radioButtonGaussianBlur = (RadioButton) rootView.findViewById(R.id.radioButtonGaussianBlur);
        RadioButton radioButtonBoxBlur = (RadioButton) rootView.findViewById(R.id.radioButtonBoxBlur);

		/*
		 * Create seekbar and its action listener
		 */
        seekBarGridSize.setMax(200);
        seekBarGridSize.setProgress(gridSize);
        seekBarGridSize.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {

                // Updates the shared variables with value of seekbar
                if (progress > 0) {
                    preferences.edit().putInt(MappishApplication.GRID_SIZE, progress).apply();
                    // Contacts the main activity callback.
                    onMapControlChange.controlChanged();
                } else {
                    progress = 1;
                    preferences.edit().putInt(MappishApplication.GRID_SIZE, progress).apply();
                    // Contacts the main activity callback.
                    onMapControlChange.controlChanged();
                }
            }


            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }
        });

        // Make the seekbar available from the drawer
        seekBarGridSize.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        // Disallow Drawer to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(true);
                        break;

                    case MotionEvent.ACTION_UP:
                        // Allow Drawer to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(false);
                        break;
                }
                return false;
            }
        });
		
		/*
		 * Create button groups and its listeners
		 */
        switch (viewColoringType) {
            case MappishApplication.VIEW_HEATMAP:
                radioButtonHeatMap.toggle();
                break;
            case MappishApplication.VIEW_COLOR_SHADES:
                radioButtonColorShade.toggle();
                break;
        }

        switch (blurType){
            case MappishApplication.BLUR_BOX:
                radioButtonBoxBlur.toggle();
                break;
            case MappishApplication.BLUR_GAUSSIAN:
                radioButtonGaussianBlur.toggle();
                break;
        }



		
		/* Listener attached to the radio buttons
		* It changes the shared prefs to two different options.
		* Finally notify the callback to the activity
		*/
        // Listener for the coloring type
        OnClickListener viewButtonListener = new OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (view.getId()) {
                    case R.id.button_view_heatmap:
                        preferences.edit().putInt(MappishApplication.MAP_COLORING_TYPE, MappishApplication.VIEW_HEATMAP).apply();
                        onMapControlChange.controlChanged();
                        break;
                    case R.id.button_view_colorshade:
                        preferences.edit().putInt(MappishApplication.MAP_COLORING_TYPE, MappishApplication.VIEW_COLOR_SHADES).apply();
                        onMapControlChange.controlChanged();
                        break;

                }

            }
        };

        // Listener for the blur type
        OnClickListener blurTypeButtonListener = new OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (view.getId()){
                    case R.id.radioButtonBoxBlur:
                        preferences.edit().putInt(MappishApplication.BLUR_TYPE, MappishApplication.BLUR_BOX).apply();
                        onMapControlChange.controlChanged();
                        break;
                    case R.id.radioButtonGaussianBlur:
                        preferences.edit().putInt(MappishApplication.BLUR_TYPE, MappishApplication.BLUR_GAUSSIAN).apply();
                        onMapControlChange.controlChanged();
                        break;
                }

            }
        };

        radioButtonBoxBlur.setOnClickListener(blurTypeButtonListener);
        radioButtonGaussianBlur.setOnClickListener(blurTypeButtonListener);
        radioButtonColorShade.setOnClickListener(viewButtonListener);
        radioButtonHeatMap.setOnClickListener(viewButtonListener);

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    // Overrides on Attach and makes sure it implements the OnMapControlChange
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // An Interface that needs to be implemented by the Activity holding the actual map
        try {
            onMapControlChange = (OnMapControlChange) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        System.out.println("KILLED ME");
    }


}

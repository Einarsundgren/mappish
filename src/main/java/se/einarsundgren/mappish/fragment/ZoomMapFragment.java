
/*
 * Copyright 2015 Einar Sundgren
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package se.einarsundgren.mappish.fragment;


import se.einarsundgren.mappish.MappishApplication;
import se.einarsundgren.mappish.R;
import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.RadioButton;

public class ZoomMapFragment extends Fragment {


			MappishApplication app;
			SharedPreferences preferences;
			int zoomLevel;
			int markerType;
			Activity parentActivity;
			OnMapControlChange onMapControlChange;

			@Override
			public View onCreateView(LayoutInflater inflater, ViewGroup container,
					Bundle savedInstanceState) {
                super.onCreateView(inflater, container, savedInstanceState);

				parentActivity = getActivity();
				preferences = parentActivity.getSharedPreferences(MappishApplication.MAPPISH_PREFERENCES, Context.MODE_PRIVATE);
				app = (MappishApplication) getActivity().getApplication();
				View rootView = inflater.inflate(R.layout.change_presentation_fragment,
						container, false);

                rootView.setBackgroundColor(Color.WHITE);
				
				markerType = preferences.getInt(MappishApplication.MARKER_TYPE, MappishApplication.MARKER_RECT);
				zoomLevel = preferences.getInt(MappishApplication.ZOOM_LEVEL, 12);
				
				RadioButton circlesButton = (RadioButton) rootView.findViewById(R.id.button_view_circles);
				RadioButton rectsButton = (RadioButton) rootView.findViewById(R.id.button_view_squares);
				

				/*
				 * Prepare the right button as toggeled. Define the action listener and 
				 * fire it up.
				 */
				switch (markerType) {
				case MappishApplication.MARKER_CIRCLE:
					circlesButton.toggle();
					break;
				case MappishApplication.MARKER_RECT:
					rectsButton.toggle();
					break;
				}
				
				OnClickListener onClickListener = new OnClickListener() {
					@Override
					public void onClick(View view) {
						switch (view.getId()){
						case R.id.button_view_circles:
							preferences.edit().putInt(MappishApplication.MARKER_TYPE, MappishApplication.MARKER_CIRCLE).apply();
							onMapControlChange.controlChanged();
							break;
						case R.id.button_view_squares:
							preferences.edit().putInt(MappishApplication.MARKER_TYPE, MappishApplication.MARKER_RECT).apply();
							onMapControlChange.controlChanged();
							break;

						}
						
					}
				};
				
				circlesButton.setOnClickListener(onClickListener);
				rectsButton.setOnClickListener(onClickListener);
				
				return rootView;
			}
			
			
			// Overrides on Attach and makes sure it implements the OnMapControlChange
			@Override
			public void onAttach(Activity activity){
		        super.onAttach(activity);

			       try {
			    	   onMapControlChange = (OnMapControlChange) activity;
			        } catch (ClassCastException e) {
			            throw new ClassCastException(activity.toString()
			                    + " must implement OnHeadlineSelectedListener");
			        }
			    

			}
			

		}


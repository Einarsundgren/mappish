
/*
 * Copyright 2015 Einar Sundgren
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package se.einarsundgren.mappish.fragment;

import se.einarsundgren.mappish.MappishApplication;
import se.einarsundgren.mappish.R;
import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.RadioButton;

public class SettingsPathsAndPlacesFragment extends Fragment {

	OnMapControlChange onMapControlChange;
	Activity parentActivity;
	MappishApplication app;
    int viewType;
    private SharedPreferences preferences;



    @Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);



		parentActivity = getActivity();
        preferences = parentActivity.getSharedPreferences(MappishApplication.MAPPISH_PREFERENCES, Context.MODE_PRIVATE);

        app = (MappishApplication) getActivity().getApplication();
		View rootView = inflater.inflate(R.layout.settings_paths_and_places_fragment,
				container, false);
		
		
		parentActivity = getActivity();
		

        rootView.setBackgroundColor(Color.WHITE);

        viewType = preferences.getInt(MappishApplication.MAP_TYPE, MappishApplication.VIEW_PATHS_AND_PLACES);
        onMapControlChange.controlChanged();




		return rootView;
	}

	@Override
	public void onResume() {
		super.onResume();

	}
	
	// Overrides on Attach and makes sure it implements the OnMapControlChange
	@Override
	public void onAttach(Activity activity){
        super.onAttach(activity);

	       try {
	    	   onMapControlChange = (OnMapControlChange) activity;
	        } catch (ClassCastException e) {
	            throw new ClassCastException(activity.toString()
	                    + " must implement OnHeadlineSelectedListener");
	        }
	    

	}

	
	
	
}

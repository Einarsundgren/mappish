
/*
 * Copyright 2015 Einar Sundgren
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package se.einarsundgren.mappish.fragment;

import java.util.ArrayList;
import java.util.List;

import org.osmdroid.DefaultResourceProxyImpl;
import org.osmdroid.ResourceProxy;
import org.osmdroid.events.MapListener;
import org.osmdroid.events.ScrollEvent;
import org.osmdroid.events.ZoomEvent;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapController;
import org.osmdroid.views.Projection;
import org.osmdroid.views.overlay.ItemizedOverlayWithFocus;
import org.osmdroid.views.overlay.OverlayItem;

import se.einarsundgren.mappish.database.MappishUIHelper;
import se.einarsundgren.mappish.HeatmapItemListener;
import se.einarsundgren.mappish.MappishApplication;
import se.einarsundgren.mappish.MappishHeatmapOverlay;
import se.einarsundgren.mappish.MappishPoint;
import se.einarsundgren.mappish.R;

import se.einarsundgren.mappish.database.MappishDB;
import se.einarsundgren.mappish.osmdroid.MappishMapView;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;

public class MapFragment extends Fragment {
		protected MappishApplication app;
		public MappishMapView mapView;
		protected static String TAG = "MapFragment";
        private MapController mapController;
		private Projection projection;
		protected ResourceProxy resProxy;
		public ItemizedOverlayWithFocus<OverlayItem> heatMap;
		protected int zoomLevel;
		private SharedPreferences preferences;
		public int scrollX;
		public int scrollY;

       // LocationManager locationManager;

		
		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle SavedInstanceState) {
            super.onCreateView(inflater, container, SavedInstanceState);
            //Log.d(TAG, "View created");
			app = (MappishApplication) getActivity().getApplication();
            MappishDB mappishDB = new MappishDB(app.getApplicationContext());

			resProxy = new DefaultResourceProxyImpl(getActivity()
					.getApplicationContext());

			View rootView = inflater.inflate(R.layout.fragment_heatmap,
					container, false);
			//getActivity();

			preferences = getActivity().getSharedPreferences(MappishApplication.MAPPISH_PREFERENCES,Context.MODE_PRIVATE);

			List<OverlayItem> overlayItems = new ArrayList<OverlayItem>();
			
			mapView = (MappishMapView) rootView.findViewById(R.id.mapfragment);
			
			heatMap = new MappishHeatmapOverlay(overlayItems,
					new HeatmapItemListener(), resProxy,
                    app,
                    mapView,getActivity());

			zoomLevel = preferences.getInt(MappishApplication.ZOOM_LEVEL, 10);

			mapView.setPrefs(preferences);
			mapView.getOverlays().add(heatMap);
			mapView.setMultiTouchControls(true);

			mapController = (MapController) mapView.getController();
			mapController.setZoom(zoomLevel);
			projection = mapView.getProjection();

			// Centers on last recorded geopoint
			if(mappishDB.getLastSaved()!=null){
                //TODO: For some reason this centers a bit off (~500m) from the actual location. Used location seems correct.
				mapController.setCenter(mappishDB.getLastSaved());

            }
            /*
            // No position saved. Find one for this view...
			else {
                String locationProvider = LocationManager.GPS_PROVIDER;
                locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

                boolean provGPS = false;
                try {
                    provGPS = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
                } catch (Exception e){
                    Log.d(TAG, "Crashed");
                }

                if (!provGPS){
                Log.d(TAG, "GPS IS OFF...");
                    MappishUIHelper.gpsOffNotification(this.app);
                }


                // This looks weird. Why are there two location requests?
                //locationManager.requestSingleUpdate(locationProvider, this, null);
                //locationManager.requestLocationUpdates(locationProvider, MappishApplication.getSleepTime()/2, 10, this);
                mapController.setCenter(new MappishPoint(0.0,0.0,(int)System.currentTimeMillis()/1000));
            }
            */
            mappishDB.close();

			// Make the gridsize adjust to the zoom level.
			int gz = (int) ((MappishApplication.MIN_GRID_SIZE/2) * Math.pow(2, (mapView.getMaxZoomLevel() - mapView.getZoomLevel())+1));
			preferences.edit().putInt(MappishApplication.GRID_SIZE, gz ).apply();


			mapView.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				mapView.touchUp = false;
				if(event.getAction() == MotionEvent.ACTION_UP ){
					mapView.getScroller().abortAnimation();
					mapView.touchUp = true;
					mapView.drawHeat();	
				}



				return false;
			}
		});

	      mapView.setMapListener(new MapListener() {   
	    	
			@Override
			public boolean onScroll(ScrollEvent event) {

				return false;
			}

			@Override
			public boolean onZoom(ZoomEvent arg0) {
				preferences.edit().putInt(MappishApplication.ZOOM_LEVEL,projection.getZoomLevel()).apply();
				//Log.d(TAG, "ZL: " +projection.getZoomLevel());
                if (preferences !=null) {
					
					// Make the gridsize adjust to the zoom level.
					int gz = (int) ((MappishApplication.MIN_GRID_SIZE/2) * Math.pow(2, (mapView.getMaxZoomLevel() - mapView.getZoomLevel())+1));
					preferences.edit().putInt(MappishApplication.GRID_SIZE, gz).apply();
				}
				mapView.drawHeat();
				return false;
			}
	    } );
			return rootView;
		}


		public MappishMapView getMapView() {
			return mapView;
		}

		public void setMapView(MappishMapView mapView) {
			this.mapView = mapView;
		}

		public MapController getMapController() {
			return mapController;
		}

		public void setMapController(MapController mapController) {
			this.mapController = mapController;
		}


    @Override
    public void onResume(){
        super.onResume();
        //Log.d(TAG, "Fragment id " +this.getId()+" resuming");

        //MappishDB mappishDB = new MappishDB(getActivity());
        //mapController.setCenter(mappishDB.getLastSaved());
        //mappishDB.close();
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
        //Log.d(TAG, "Fragment id " +this.getId()+" destroy View");

        //MappishDB mappishDB = new MappishDB(getActivity());
        //mapController.setCenter(mappishDB.getLastSaved());
        //mappishDB.close();
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
       // Log.d(TAG, "Fragment id " +this.getId()+" destroyed");

        //MappishDB mappishDB = new MappishDB(getActivity());
        //mapController.setCenter(mappishDB.getLastSaved());
        //mappishDB.close();
    }

    @Override
    public void onDetach(){
        super.onDetach();
        //Log.d(TAG, "Fragment id " +this.getId()+" detached");

        //MappishDB mappishDB = new MappishDB(getActivity());
        //mapController.setCenter(mappishDB.getLastSaved());
        //mappishDB.close();
    }



}
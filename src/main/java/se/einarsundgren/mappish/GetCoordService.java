
/*
 * Copyright 2015 Einar Sundgren
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package se.einarsundgren.mappish;

import se.einarsundgren.mappish.database.MappishDB;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

public class GetCoordService extends Service implements LocationListener {

    private LocationManager locationManager;
    private boolean isGPOnSNotified = false;

    protected String locationProvider = LocationManager.GPS_PROVIDER;
    String TAG = "GetCoordService";

    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int id) {
        String locationProvider = LocationManager.GPS_PROVIDER;
        Location lastKnownLocation;
        MappishDB mappishDB = new MappishDB(this);

        // Log.d(TAG, "Trying to update coords.");

        // createLocationManager();
        if (locationManager == null) {
            // Log.d(TAG, "Trying to recreate location manager....");
            createLocationManager();
        } else {
            locationManager.requestSingleUpdate(locationProvider,this,null);
        }

        // Log.d(TAG, "Checking if GPS provider is on.");
        //Check if location provider is on.
        if ( locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
                ) {
            // Log.d(TAG, "Checking if LM is null.");

            // Check if we have any location stored
            if (locationManager != null) {
                // Log.d(TAG, "Getting last known location.");
                // Log.d(TAG, "Last known location fixed from: " + locationManager.getLastKnownLocation(locationProvider));

                lastKnownLocation = locationManager.getLastKnownLocation(locationProvider);

                if (lastKnownLocation != null) {
                    long saveResult = mappishDB.saveCoords(lastKnownLocation.getLatitude(), lastKnownLocation.getLongitude());

                   //  Log.d(TAG, "Updated coords lat: " + lastKnownLocation.getLatitude() + " lon: " + lastKnownLocation.getLongitude());
                    //Log.d(TAG, "Saved at: " + lastKnownLocation.getTime());

                    if (saveResult < 0) {
                      //  Log.d(TAG, "An error occured while storing the value to the db");
                    } else {
                       // Log.d(TAG, "Result stored as " + saveResult);
                    }

                } else {
                    // Log.d(TAG, "Mappish did not update cordinates. lastKnownLocation is null");
                }
            } else {
                // Log.d(TAG, "Last known location was null.");
            }

        } else {
           // Log.d(TAG, "GPS Service was off.");
        }


        /*

        if (!isGPOnSNotified){//) {
            Log.d(TAG, "Sending notificaiton.");
            MappishUIHelper.gpsOffNotification(this.getApplicationContext());
            isGPOnSNotified = true;
        } else {
            Log.d(TAG, "Mappish did not update coordinates. No GPS service.");
        }
        */

        mappishDB.close();
        //locationManager.removeUpdates(this);
        return START_NOT_STICKY;

    }

    @Override
    public void onCreate() {
        createLocationManager();
    }

    @Override
    public void  onDestroy(){
        super.onDestroy();
        Log.d(TAG, "Service destroyed");
        locationManager.removeUpdates(this);
    }

    @Override
    public void onLocationChanged(Location location) {
        /*
        Log.d(TAG, "locations changed");
        Log.d(TAG, "lat: " + location.getLatitude());
        Log.d(TAG, "long: " + location.getLongitude());
        Log.d(TAG, "Accuracy: " + location.getAccuracy());
        */
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }


    private void createLocationManager() {
        // Creates a location manager
        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        locationManager.requestSingleUpdate(locationProvider,this,null);
        //locationManager.requestLocationUpdates(locationProvider, MappishApplication.getSleepTime() / 2, 10, this);
    }
}

/*
 * Copyright 2015 Einar Sundgren
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package se.einarsundgren.mappish.database;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.provider.Settings;
//import android.support.v4.app.NotificationCompat;

import se.einarsundgren.mappish.MappishApplication;
import se.einarsundgren.mappish.R;

/**
 * Created by Einar Sundgren on 2015-02-22.
 * This class contains minor helper functions for the UI.
 */
public class MappishUIHelper {

/*
public static void gpsOffNotification(Context context) {

    Notification.Builder mBuilder =
            new Notification.Builder(context)
                    .setSmallIcon(R.mipmap.notification_icon)
                    .setContentTitle("Mappish can't see where you are")
                    .setContentText("Try turning on your gps.");
    NotificationManager mNotifyMgr =
            (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
    Intent resultIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
    PendingIntent resultPendingIntent =
            PendingIntent.getActivity(
                    context,
                    0,
                    resultIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT
            );

    mBuilder.setContentIntent(resultPendingIntent);
    int mNotificationId = MappishApplication.GPS_NOTIFICATION_ID;
    mNotifyMgr.notify(mNotificationId, mBuilder.build());
}
*/
}

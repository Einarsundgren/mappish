
/*
 * Copyright 2015 Einar Sundgren
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package se.einarsundgren.mappish.database;

import java.util.ArrayList;

import org.osmdroid.util.BoundingBoxE6;
import org.osmdroid.util.GeoPoint;

import se.einarsundgren.gis.GISHelper;
import se.einarsundgren.mappish.MappishPoint;
import se.einarsundgren.mappish.MatrixHelper;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import android.util.Log;

public class MappishDB {
	
	@SuppressWarnings("unused")
	private final static String TAG = "DBClass";
	private static final boolean DEBUG = false;

    /*
    Static types for the queries.
     */

    private static String sqlWhere = "lat < ? AND lat > ? AND lon > ? AND lon < ? AND time > ? AND time < ?";
    //private static String sqlGroup = "Cast(lat*1000 AS INTEGER), CAST(lon * 10000 AS INTEGER);";
    private static String sqlGroup = "time,lat,lon;";

    // Casting used for the mapping function to not exceed four decimal points.
    private static String [] calculatedColumns = {
            MappishDBHelper.COLUMN_LAT,
            MappishDBHelper.COLUMN_LON,
            MappishDBHelper.COLUMN_TIME,
            "COUNT(_id)"
    };
    private static String [] allColumns = {
            MappishDBHelper.COLUMN_LAT,
            MappishDBHelper.COLUMN_LON,
            MappishDBHelper.COLUMN_TIME};

	private SQLiteDatabase db;
	private ContentValues values = new ContentValues();


	public MappishDB(Context context) {
		super();
        MappishDBHelper dbHelper = new MappishDBHelper(context);
		db = dbHelper.getWritableDatabase();
		//dbHelper.exportDB();
	}
	/*
	Returns the row ID of the newly inserted row, or -1 if an error occurred (Same as SQLiteDatabase.insert())
	@return long
	 */
	public long saveCoords(Double lat, Double lon){
		values.put(MappishDBHelper.COLUMN_LAT, lat);
		values.put(MappishDBHelper.COLUMN_LON, lon);
		values.put(MappishDBHelper.COLUMN_TIME, System.currentTimeMillis()/1000);
        return db.insert(MappishDBHelper.TABLE_POSITIONS, null, values);
	}
	
	public String getAllCoords(){
		String tables = "";
		Cursor cursor = db.query(MappishDBHelper.TABLE_POSITIONS, 
				allColumns, 
				null, // Selection (Where)
				null, // selectionArgs (?)
				null, // group By
				null, // having
				null);// order by
		
	    cursor.moveToFirst();
	    while (!cursor.isAfterLast()) {
	    	String col = "";
	    	for (int i = 0; i< cursor.getColumnCount();i++){
	    	  col += cursor.getString(i)+" ";
	      }
	    	tables += col +"\n";

	      cursor.moveToNext();
	    }
        cursor.close();
		return tables;
	}
	
	
	
	public ArrayList<MappishPoint> getPoints(){
		ArrayList<MappishPoint> mappishPoints = new ArrayList<MappishPoint>();
		
		Cursor cursor = db.query(MappishDBHelper.TABLE_POSITIONS, 
				allColumns,
				null, 
				null, 
				null, 
				null, 
				null);
		
	    cursor.moveToFirst();
	    while (!cursor.isAfterLast()) {
	    		mappishPoints.add(new MappishPoint(cursor.getFloat(0), cursor.getFloat(1), cursor.getInt(2)));
	      cursor.moveToNext();
	    }
        cursor.close();
		return mappishPoints;
	}
	

    /*
    Searches the db for samples points saved within the bounding box of the screen and maps them to a grid of size n to be used
    as the overlay.
     */
	public MappishPoint[][] getPointsMapped (BoundingBoxE6 box, int gridSize, int startTimeStamp, int endTimeStamp)
	{

		long startTime;

        // The box limits query to what is actually on the screen
		GeoPoint ul = new GeoPoint(box.getLatNorthE6(), box.getLonWestE6());
		GeoPoint lr = new GeoPoint(box.getLatSouthE6(), box.getLonEastE6());
		
		//System.out.println("Querying from " + startTimeStamp + " to: "+ endTimeStamp);

		int eastToWestMeters = GISHelper.eastToWestMeters(box);
		int northToSouthMeters = GISHelper.nortToSouthMeters(box);
		
		//startTime = System.nanoTime();
		MappishPoint[][] matrix = MatrixHelper.createMatrix(eastToWestMeters, northToSouthMeters, gridSize, true);
		if (DEBUG) System.out.println("Matrix creation: " + (System.nanoTime()-startTime));
		

		for(int i=0;i<matrix.length;i++){
			for(int j = 0; j<matrix[0].length; j++){
				matrix[i][j] = new MappishPoint(0, 0, 0, 0);
			}
		}
		if (DEBUG) System.out.println("Matrix initialization: " + (System.nanoTime()-startTime));
		
		
		if (DEBUG) startTime = System.nanoTime();
		String[] args = {""+ul.getLatitude(),""+lr.getLatitude(), 
				""+ul.getLongitude(),""+lr.getLongitude(),
				""+startTimeStamp,""+endTimeStamp};

        /*
        Using the classes statics to define the query
         */
		Cursor cursor = db.query(MappishDBHelper.TABLE_POSITIONS, 
				MappishDB.calculatedColumns,
				MappishDB.sqlWhere,
				args,
                //"lat, lon;",
				MappishDB.sqlGroup,
                null,
				null);
		
		if (DEBUG) System.out.println("Query: " + (System.nanoTime()-startTime));
		
	    cursor.moveToFirst();

        // Log.d(TAG, "Found " + cursor.getCount() + " items");

	    if (DEBUG) startTime = System.nanoTime();
	    long s2, sum = 0, count = 0;
	    while (cursor.moveToNext()) {

	    	if (DEBUG) count ++;
	    	if (DEBUG) s2 = System.nanoTime();

            /*
            Casting backwards and forwards to round the float to four decimal points since
            more precision is unnecessary and this makes the mapping easier.
             */
	    	double lat = (double)    ((int)(cursor.getFloat(0)*10000)) /10000;
	    	double lon = (double)    ((int)(cursor.getFloat(1)*10000)) /10000;
            cursor.getInt(2); //TODO: Just to access all of the fields. Not certain if this is necessary

                /*
                 Maps the lat/lon coordinate to the matrix grid representing the map.
                 This is the most timeconsuming part.
                 */
				int[] matrixMap = MatrixHelper.mapToMatrix(
						(double)box.getLatNorthE6() / 1000000, 
						(double)box.getLonWestE6()  / 1000000, 
						lat, 
						lon,
						gridSize);

			//Add the strength of each point to each other
			matrix[matrixMap[0]][matrixMap[1]].strength += cursor.getInt(3);	
			if (DEBUG) sum += (System.nanoTime()-s2);
			
	    }
	      
	    if (DEBUG) System.out.println("Mapping avg time: " + sum/count + " times: " + count);
        cursor.close();
		return matrix;
	}


    /*
    * Searches the bounding box of the screen but returns the points as a chronologically ordered arrayList of
    * Mappishpoints.
     */
    public ArrayList<MappishPoint> getPointsUnMapped (BoundingBoxE6 box, int startTimeStamp, int endTimeStamp)
    {

        // The box limits query to what is actually on the screen
        GeoPoint ul = new GeoPoint(box.getLatNorthE6(), box.getLonWestE6());
        GeoPoint lr = new GeoPoint(box.getLatSouthE6(), box.getLonEastE6());

        ArrayList<MappishPoint> pointList = new ArrayList<MappishPoint>();

        //System.out.println("Querying unmapped from " + startTimeStamp + " to: "+ endTimeStamp);

        String[] args = {""+ul.getLatitude(),""+lr.getLatitude(),
                ""+ul.getLongitude(),""+lr.getLongitude(),
                ""+startTimeStamp,""+endTimeStamp};

        Cursor cursor = db.query(MappishDBHelper.TABLE_POSITIONS,
                MappishDB.calculatedColumns,
                MappishDB.sqlWhere,
                args,
                MappishDB.sqlGroup,
                null,
                null);

        cursor.moveToFirst();

        //Log.d(TAG, "Found " + cursor.getCount() + " items");



        while (cursor.moveToNext()) {
            double lat = (double)    ((int)(cursor.getFloat(0)*10000)) /10000;
            double lon = (double)    ((int)(cursor.getFloat(1)*10000)) /10000;
            int timeStamp = cursor.getInt(2);

            pointList.add(new MappishPoint(lat,lon,timeStamp));

        }
        cursor.close();


        return pointList;
    }

	
	/**
	 * Gets all coordinates that are within the upper left and lower right bounding box.
	 * @param ul Upper left corner of bounding box
	 * @param lr Lower right corner of bounding box
	 * @return An arraylist of saved points within the supplied area
	 */
	
	public ArrayList<MappishPoint> getPoints(GeoPoint ul, GeoPoint lr){
		ArrayList<MappishPoint> mappishPoints = new ArrayList<MappishPoint>();
		String[] args = {""+ul.getLatitude(),""+lr.getLatitude(),""+ul.getLongitude(),""+lr.getLongitude()};
		Cursor cursor = db.query(MappishDBHelper.TABLE_POSITIONS, 
				allColumns,
				"lat < ? AND lat > ? AND lon > ? AND lon < ?", 
				args, 
				null, 
				null, 
				null);
		
	    cursor.moveToFirst();
	    while (!cursor.isAfterLast()) {
	    		mappishPoints.add(new MappishPoint(cursor.getFloat(0), cursor.getFloat(1), cursor.getInt(2)));
	      cursor.moveToNext();
	    }
        cursor.close();
		return mappishPoints;
	}
	
	
	public MappishPoint getLastSaved(){		
		MappishPoint mappishPoint;
		Cursor cursor = db.query(
				MappishDBHelper.TABLE_POSITIONS,
                MappishDB.calculatedColumns,
				null,
				null,
                null,
                //MappishDB.sqlGroup,
				null,
                MappishDBHelper.COLUMN_TIME + " ASC limit 1"
        );
		
	    if (cursor.moveToFirst()){
	    	    		mappishPoint = new MappishPoint(cursor.getFloat(0), cursor.getFloat(1), cursor.getInt(2));
                        //Log.d(TAG, "Centerpoint lat: " + mappishPoint.getLatitude() + " lon: " + mappishPoint.getLongitude() + " time: " +mappishPoint.getTime()+" Itemos found: "+ cursor.getCount());
	    	    		return mappishPoint;
            //return new MappishPoint(57.69195227, 11.92344486,(int)System.currentTimeMillis()/1000);
	    } else {
            //System.out.println("Error reading first row");
        }
        cursor.close();
	    return null;
	}
	
	public void delete(){
		db.delete(MappishDBHelper.TABLE_POSITIONS, null, null);

	}

    public void close(){
        if (db!=null)db.close();
    }
	

}

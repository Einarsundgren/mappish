
/*
 * Copyright 2015 Einar Sundgren
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package se.einarsundgren.mappish.database;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.util.Log;

public class MappishDBHelper extends SQLiteOpenHelper{

	private static final String TAG = "DBHelper";
	
    private static final String DATABASE_NAME = "MappishDB";   
    
    private static final int DATABASE_VERSION = 2;
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_LAT = "lat";
    public static final String COLUMN_LON = "lon";
    public static final String COLUMN_TIME = "time";
    public static final String TABLE_POSITIONS ="MappishPositions";
    
    private static final String DATABASE_CREATE = "create table "+TABLE_POSITIONS+" ("+COLUMN_ID+" integer primary key,"+COLUMN_LAT+" real , "+COLUMN_LON+" real , "+COLUMN_TIME+" integer);";    
	public MappishDBHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(DATABASE_CREATE);
		// Log.d(TAG, "onCreated sql: " + DATABASE_CREATE);
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		
	}
	
    /***
     * Only used during development
     */	
    @SuppressWarnings("resource")
	public static void exportDB() {
        // TODO Auto-generated method stub

        try {
            File sd = Environment.getExternalStorageDirectory();
            File data = Environment.getDataDirectory();

            if (sd.canWrite()) {
                String  currentDBPath= "//data//" + "se.einarsundgren.mappish"
                        + "//databases//" + DATABASE_NAME;
                String backupDBPath  = "/"+DATABASE_NAME;
                File currentDB = new File(data, currentDBPath);
                File backupDB = new File(sd, backupDBPath);

                FileChannel src = new FileInputStream(currentDB).getChannel();
                FileChannel dst = new FileOutputStream(backupDB).getChannel();
                dst.transferFrom(src, 0, src.size());
                src.close();
                dst.close();
                //System.out.println(backupDB.toString());
                

            }
        } catch (Exception e) {

        	//System.out.println(e.toString());
        	e.printStackTrace();

        }
    }


}

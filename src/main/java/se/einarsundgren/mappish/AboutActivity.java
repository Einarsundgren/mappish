/*
 * Copyright 2015 Einar Sundgren
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package se.einarsundgren.mappish;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import se.einarsundgren.mappish.database.MappishDB;
import se.einarsundgren.mappish.database.MappishDBHelper;

/**
 * Created by Einar on 2015-02-22.
 * This class is just showing the about and licensing information
 */
public class AboutActivity extends Activity{


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);


    }

    public void emptyDatabase(View view){
        //MappishDBHelper.exportDB();

        MappishDB mappishDB = new MappishDB(getApplicationContext());
        mappishDB.delete();

    }
}

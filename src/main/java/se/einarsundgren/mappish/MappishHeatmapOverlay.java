
/*
 * Copyright 2015 Einar Sundgren
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package se.einarsundgren.mappish;

import java.util.ArrayList;
import java.util.List;

import org.osmdroid.ResourceProxy;
import org.osmdroid.util.BoundingBoxE6;
import org.osmdroid.util.GeoPoint;

import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.ItemizedOverlayWithFocus;
import org.osmdroid.views.overlay.OverlayItem;

import se.einarsundgren.gis.GISHelper;
import se.einarsundgren.mappish.database.MappishDB;
import se.einarsundgren.mappish.osmdroid.MappishMapView;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.Rect;
import android.util.Log;


public class MappishHeatmapOverlay extends ItemizedOverlayWithFocus<OverlayItem> {

    final static String TAG = "Overlay";
    private    List<OverlayItem> aList = null;
    private Activity activity;
    private SharedPreferences preferences;
    private Context context;

    private int gridSize;
    private float innerRadius;

    private long startTestTime, endTestTime, testTimeMillis;
    private int counter;

    public MappishHeatmapOverlay(Context ctx, OnItemGestureListener<OverlayItem> aOnItemTapListener) {
        super(ctx, null, aOnItemTapListener);

    }


    public MappishHeatmapOverlay(
            List<OverlayItem> aList,
            org.osmdroid.views.overlay.ItemizedIconOverlay.OnItemGestureListener<OverlayItem> aOnItemTapListener,
            ResourceProxy pResourceProxy,
            Context context,
            MappishMapView mapView,
            Activity activity
    ) {
        super(aList, aOnItemTapListener, pResourceProxy);
        this.context = context;
        this.activity = activity;
        // Initiate prefs
        preferences = activity.getSharedPreferences(MappishApplication.MAPPISH_PREFERENCES, Activity.MODE_PRIVATE);
        gridSize = preferences.getInt(MappishApplication.GRID_SIZE, 20);
        if (gridSize < 10) gridSize = 10;


    }

    @Override
    public void draw(Canvas canvas, MapView mapView, boolean shadow) {
        counter = 0;
        // Redraw heatmap overlay on touchup
        super.draw(canvas, mapView, shadow);


        if (((MappishMapView) mapView).touchUp) {

            //Log.d(TAG, "ZL from overlay" + mapView.getProjection().getZoomLevel());
            // Saving zoom level from the mapview retreived from tis overlay since the overriden method in MappishMapView does not work.
            preferences.edit().putInt(MappishApplication.ZOOM_LEVEL, mapView.getProjection().getZoomLevel()).apply();
            // Update prefs for the drawing.
            gridSize = preferences.getInt(MappishApplication.GRID_SIZE, 20);
            if (gridSize < 10) gridSize = 10;

            MappishPoint tempPoint;
            BoundingBoxE6 box = mapView.getBoundingBox();

            //Search time from - to
            int startTimeStamp = preferences.getInt(MappishApplication.QUERY_START_TIME, 0);
            int endTimeStamp = preferences.getInt(MappishApplication.QUERY_END_TIME, (int) (System.currentTimeMillis()/1000L));

            // Initialize db for this session
            MappishDB db =  new MappishDB(context.getApplicationContext());

            // Points used to translate between projections.
            Point startScreenPts = new Point();

            /*
                Gets the point of the last position. That is the center point of the current projection.
                This is used to calculate radiuses etc since it will differ depending on where we are on earth,
             */
            GeoPoint lastPosition;
            lastPosition = db.getLastSaved();
            if (lastPosition==null) return;


            switch((preferences.getInt(MappishApplication.MAP_TYPE,MappishApplication.VIEW_PATHS_AND_PLACES))){
            /*
            Can this block be refectored to a separate class/function?
            ******************************
             */
            case MappishApplication.VIEW_PLACES:

            // Set the radius to half the gridsize so one circle fits in one grid cell.
            innerRadius = GISHelper.metersToRadius((float) (gridSize / 2), mapView, lastPosition.getLatitude());
            MappishPoint[][] matrix = db.getPointsMapped(
                    box, // Bounding box in coordinates
                    gridSize,
                    startTimeStamp,
                    endTimeStamp
            ); // Definition of matrix to map to.


            // Apply filter to overlay.
            int blurType = preferences.getInt(MappishApplication.BLUR_TYPE, MappishApplication.BLUR_GAUSSIAN);
            if (blurType == MappishApplication.BLUR_BOX) {

                // Box blur in three passes
                for (int i = 0; i < 3; i++)
                    matrix = MatrixHelper.boxBlur(matrix);

            } else if (blurType == MappishApplication.BLUR_GAUSSIAN) {
                matrix = MatrixHelper.gaussianBlur(matrix);
            }


            // Find highest value. (Can this be optimized by moved into the blur functions?)
            int highestStrength = 0;
            for (int i = 0; i < matrix.length; i++) {
                for (int j = 0; j < matrix[i].length; j++) {
                    if (matrix[i][j].strength > highestStrength) {
                        highestStrength = matrix[i][j].strength;
                    }
                }
            }


            //Paint for the dots from the matrix
            Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
            paint.setStyle(Paint.Style.FILL_AND_STROKE);
            paint.setStrokeCap(Paint.Cap.ROUND);
            paint.setStrokeWidth(1);
            paint.setColor(Color.BLUE);

            // Normalizing the values to be within 0-255
            double normalizer = 1.0;
            if (highestStrength > 0) {
                normalizer = 255.0 / highestStrength;
                /*
                Log.d(TAG, " Strength normalized to: " + normalizer);
                Log.d(TAG, " Debug counter strangely at: " + counter++);
                */
            }

            // Reversing the haversine with an offset.
            // http://gis.stackexchange.com/questions/2951/algorithm-for-offsetting-a-latitude-longitude-by-some-amount-of-meters
            for (int i = 0; i < matrix.length; i++) {
                for (int j = 0; j < matrix[i].length; j++) {
                    if (matrix[i][j].strength > 0) {
                        //Position, decimal degrees
                        double lat = ((double) (box.getLatNorthE6() / 100)) / 10000;
                        double lon = ((double) (box.getLonWestE6() / 100)) / 10000;
                        GeoPoint gp = GISHelper.getCoordsAtOffset(new GeoPoint(lat, lon), i * gridSize, -j * gridSize);
                        tempPoint = new MappishPoint(gp.getLatitude(), gp.getLongitude(), 123, matrix[i][j].strength);
                        mapView.getProjection().toPixels(tempPoint, startScreenPts);

                                /*
                                * Switch case for selector of map color type
				                */

                                switch (preferences.getInt(MappishApplication.MAP_COLORING_TYPE, MappishApplication.VIEW_COLOR_SHADES)) {
                                    case MappishApplication.VIEW_COLOR_SHADES:
                                       // Log.d(TAG, "Colorshades");
                                        double t = normalizer * tempPoint.strength;
                                        paint.setAlpha((int) t);
                                        break;

                                    case MappishApplication.VIEW_HEATMAP:
                                       // Log.d(TAG, "Heatmap");
                                        // Change the hue value to create the heatmap effect
                                        float norm = 200/highestStrength;
                                        float v = (float)(normalizer * tempPoint.strength);
                                        float hsv[] = new float[3];
                                        hsv[0]=200-v;
                                        hsv[1]=100;
                                        hsv[2]=100;
                                        paint.setColor(Color.HSVToColor(hsv));

                                        break;


                                }

                        // Switch case for type of marker displayed
                        switch (preferences.getInt(MappishApplication.MARKER_TYPE, MappishApplication.MARKER_CIRCLE)) {

                            // Draw circles
                            case MappishApplication.MARKER_CIRCLE:
                                canvas.drawCircle(startScreenPts.x, startScreenPts.y, innerRadius, paint);
                                //Log.d(TAG, "Circle");
                                break;

                            // Draw rectagles
                            case MappishApplication.MARKER_HEXAGON:
                                Rect rect = new Rect(
                                        startScreenPts.x - (int) innerRadius,
                                        startScreenPts.y - (int) innerRadius,
                                        startScreenPts.x + (int) innerRadius,
                                        startScreenPts.y + (int) innerRadius);
                                canvas.drawRect(rect, paint);
                                break;

                            case MappishApplication.MARKER_RECT:
                                Path path = new Path();
                                int x = startScreenPts.x;
                                int y = startScreenPts.y;

                                double side = (innerRadius) * Math.sqrt(3);
                                double h = Math.sin(Math.toRadians(30)) * side;
                                int rside = (int) Math.round(side);
                                int rh = (int) Math.round(h);
                                int rRadius = (int) Math.round(innerRadius);

                                y -= rRadius;
                                path.moveTo(x, y);

                                x += rRadius;
                                y -= rh;
                                path.lineTo(x, y);

                                y += rside;
                                path.lineTo(x, y);

                                x -= rRadius;
                                y += rh;
                                path.lineTo(x, y);

                                x -= rRadius;
                                y -= rh;
                                path.lineTo(x, y);

                                y -= rside;
                                path.lineTo(x, y);

                                y -= rh;
                                x += rRadius;
                                path.lineTo(x, y);


                                canvas.drawPath(path, paint);
                                break;
                        }

                    }
                }

            }


            /*
            // Paint for the reference point
            Paint paintOuter = new Paint(Paint.ANTI_ALIAS_FLAG);
            paintOuter.setColor(Color.RED);
            paintOuter.setStyle(Paint.Style.STROKE);
            paintOuter.setStrokeCap(Paint.Cap.ROUND);
            paintOuter.setStrokeWidth(1);


            // Paint reference points at exact coordinates as stored.
            boolean ref = false;
            if (ref) {
                for (int i = 0; i < 0;//mappishPoints.size();
                     i++) {

                    double tempLat = (double) box.getLatNorthE6() / 1000000;
                    double tempLon = (double) box.getLonWestE6() / 1000000;


                    tempPoint = new MappishPoint(tempLat, tempLon, 123);
                    mapView.getProjection().toPixels(tempPoint, startScreenPts);
                    canvas.drawCircle(startScreenPts.x, startScreenPts.y, innerRadius, paintOuter);
                }
            }
            */
            /*
            End of block to break out
            *****************
             */
           break;

                case MappishApplication.VIEW_PATHS_AND_PLACES:
                // Log.d(TAG, "Placeholder for paths algorithm");
                ArrayList<MappishPoint> points = db.getPointsUnMapped(box,startTimeStamp,endTimeStamp);
                 Point startPoint = new Point();
                 Point endPoint = new Point();
                 paint = new Paint();

                 Paint linePaint = new Paint();
                 linePaint.setColor(Color.RED);


                 ArrayList<ArrayList<MappishPoint>> clusters = new ArrayList<ArrayList<MappishPoint>>();

                 innerRadius = GISHelper.metersToRadius(10, mapView, lastPosition.getLatitude());
                 MappishPoint previous = null;

                 int clustersFound = 0;
                 boolean isPath = true;


                for (int i = 0; i <points.size(); i ++) {

                    paint.setColor(Color.RED);
                    MappishPoint current = points.get(i);
                    mapView.getProjection().toPixels(current, endPoint);

                    // Make sure this is not the first point
                    if (previous!=null) {
                        // Check if the user has moved > 50m Thats considered movement... 0.05Km
                        // And that the dots are consecutive, that is not less than 100 sec apart.
                      //  Log.d(TAG, "Time between recordings: " + current.getTime());
                        if ((GISHelper.haversin(previous.getLatitude(), previous.getLongitude(), current.getLatitude(), current.getLongitude()) > .050)
                           &&
                        (current.getTime()-previous.getTime() < 120))
                        {

                            isPath = true;
                            mapView.getProjection().toPixels(previous, startPoint);
                            linePaint.setStrokeWidth(innerRadius/2);
                            canvas.drawLine(startPoint.x, startPoint.y, endPoint.x, endPoint.y, linePaint);
                        } else {
                            //Count numbers of clusters.
                            if (isPath) {
                                clusters.add(new ArrayList<MappishPoint>());
                                clustersFound++;
                            }

                           // Log.d(TAG, "Clusters: " + clustersFound);
                            clusters.get(clustersFound-1).add(current);
                            isPath = false;



                            //paint.set
                            //paint.setColor(Color.BLUE);
                            //canvas.drawCircle(endPoint.x, endPoint.y, innerRadius, paint);

                        }
                    }


                    previous = current;
                }
                    Point clusterPoint = new Point();

                    for (int i = 0 ; i < clusters.size(); i ++){
                        for(int j = 0; j <clusters.get(i).size(); j++){
                            if (i%2==1) paint.setColor(Color.GREEN); else paint.setColor(Color.MAGENTA);
                            mapView.getProjection().toPixels(clusters.get(i).get(j), endPoint);
                            canvas.drawCircle(endPoint.x, endPoint.y, innerRadius, paint);
                        }
                    }
                    break;
        }
            db.close();
        }


    }

	
	/*
	 * Maths for a hexagonal 
	 * h = sin( 30°) * s
	 * r = cos( 30°) * s
	 * b = s + 2 * h
	 * a = 2 * r
	 * 
	 */

    public Path makeHexagon(MappishPoint center, double radius, MappishMapView mapView) {
        Path path = new Path();
        Point screenPts = new Point();
        GeoPoint geoPoint;

        double side = (2 * radius) * Math.sqrt(3);
        double h = Math.sin(Math.toRadians(30)) * side;

        geoPoint = GISHelper.getCoordsAtOffset(center, 0, -radius);
        mapView.getProjection().toPixels(geoPoint, screenPts);
        path.moveTo(screenPts.x, screenPts.y);

        geoPoint = GISHelper.getCoordsAtOffset(geoPoint, radius, h);
        mapView.getProjection().toPixels(geoPoint, screenPts);
        path.lineTo(screenPts.x, screenPts.y);

        geoPoint = GISHelper.getCoordsAtOffset(geoPoint, 0, h);
        mapView.getProjection().toPixels(geoPoint, screenPts);
        path.lineTo(screenPts.x, screenPts.y);

        geoPoint = GISHelper.getCoordsAtOffset(geoPoint, -radius, h);
        mapView.getProjection().toPixels(geoPoint, screenPts);
        path.lineTo(screenPts.x, screenPts.y);

        geoPoint = GISHelper.getCoordsAtOffset(geoPoint, -radius, -h);
        mapView.getProjection().toPixels(geoPoint, screenPts);
        path.lineTo(screenPts.x, screenPts.y);

        geoPoint = GISHelper.getCoordsAtOffset(geoPoint, 0, -h);
        mapView.getProjection().toPixels(geoPoint, screenPts);
        path.lineTo(screenPts.x, screenPts.y);

        geoPoint = GISHelper.getCoordsAtOffset(geoPoint, radius, -h);
        mapView.getProjection().toPixels(geoPoint, screenPts);
        path.lineTo(screenPts.x, screenPts.y);


        return path;

    }

}

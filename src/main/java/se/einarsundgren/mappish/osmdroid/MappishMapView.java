
/*
 * Copyright 2015 Einar Sundgren
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package se.einarsundgren.mappish.osmdroid;
/**
 * This class overrides some functions in OSMdroids mapview.
 */

import org.osmdroid.ResourceProxy;
import org.osmdroid.tileprovider.MapTileProviderBase;
import org.osmdroid.views.MapView;
import se.einarsundgren.mappish.MappishApplication;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class MappishMapView extends MapView {

	public boolean freeScroll;
	public boolean touchUp = true;
	
	MappishApplication app;
    SharedPreferences preferences;


	public MappishMapView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}

	public MappishMapView(Context context, int tileSizePixels) {
		super(context, tileSizePixels);
		// TODO Auto-generated constructor stub
	}

	public MappishMapView(Context context, int tileSizePixels,
			ResourceProxy resourceProxy) {
		super(context, tileSizePixels, resourceProxy);
		// TODO Auto-generated constructor stub
	}

	public MappishMapView(Context context, int tileSizePixels,
			ResourceProxy resourceProxy, MapTileProviderBase aTileProvider) {
		super(context, tileSizePixels, resourceProxy, aTileProvider);
		// TODO Auto-generated constructor stub
	}

	public MappishMapView(Context context, int tileSizePixels,
			ResourceProxy resourceProxy, MapTileProviderBase aTileProvider,
			Handler tileRequestCompleteHandler) {
		super(context, tileSizePixels, resourceProxy, aTileProvider,
				tileRequestCompleteHandler);
		
		// TODO Auto-generated constructor stub
	}

	public MappishMapView(Context arg0, int arg1, ResourceProxy arg2,
			MapTileProviderBase arg3, Handler arg4, AttributeSet arg5) {
		super(arg0, arg1, arg2, arg3, arg4, arg5);
		// TODO Auto-generated constructor stub
	}

	
	@Override
	public int getMinZoomLevel(){
			return 7;
			
	}

// Candidate for removal. Does not seem to do what I require
    /*
	@Override 
	public void onSizeChanged(int w, int h, int oldw, int oldh){
		super.onSizeChanged(w,h,oldw,oldh);
		drawHeat();

	}
	*/
	
	public void setApp(MappishApplication app) {
		this.app = app;
	}
	
	public void setPrefs(SharedPreferences preferences){
		this.preferences = preferences;
	}
	
	
	@Override
	public boolean onTouchEvent(MotionEvent event){
		boolean sup;
        sup = super.onTouchEvent(event);


        return sup;

	}
	
	
	/**
	 * Sets flag on the proper overlay to draw the heatmap.
	 * And invalidates the old view.
	 */
	public void drawHeat(){
		
				touchUp = true;
				invalidate();

	}



}

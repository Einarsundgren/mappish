
/*
 * Copyright 2015 Einar Sundgren
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package se.einarsundgren.mappish;

import org.osmdroid.views.MapController;
import org.osmdroid.views.MapView;

import se.einarsundgren.mappish.database.MappishDB;
import se.einarsundgren.mappish.fragment.SettingsPathsAndPlacesFragment;
import se.einarsundgren.mappish.fragment.SettingsHeatmapFragment;
import se.einarsundgren.mappish.fragment.MapFragment;
import se.einarsundgren.mappish.fragment.SettingsTimeDateFragment;

import android.app.ActionBar;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.Menu;
import android.view.MenuItem;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends Activity implements
        se.einarsundgren.mappish.fragment.OnMapControlChange {


    public static final String MAPPISH_MAIN_PREFS = "MappishMainPrefs";
    // Fragment used to draw the map.
    MapFragment mapFragment; // Main area showing map
    // Sub area showing settings for the main area.
    Fragment timeDatefragment;
    Fragment settingsHeatmapfragment;
    Fragment settingsPathsAndPlacesFragment;
    private final String TAG = "MainActivity";
    private final String FRAG_TAG = "MapControlFragment";


    Button centerButton;
    Menu menu;
    // For the creation of the main fragment.

    SharedPreferences preferences;
    MapView mapView;
    MappishApplication app;

    //   private DrawerLayout mDrawerLayout;
    //   private ActionBarDrawerToggle mDrawerToggle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) Log.d(TAG, "Fresh start...");


        // Creates a structure of shared objects on an application level.
        app = (MappishApplication) this.getApplication();
        preferences = getSharedPreferences(MappishApplication.MAPPISH_PREFERENCES, Activity.MODE_PRIVATE);

        Intent updaterServiceIntent = ((MappishApplication) getApplication()).getUpdaterServiceIntent();
        if (updaterServiceIntent == null) {
            updaterServiceIntent = new Intent(this, UpdaterService.class);
            ((MappishApplication) getApplication()).setUpdaterServiceIntent(updaterServiceIntent);
        }

        if (!preferences.getBoolean(MappishApplication.PREF_RECORD_POS, false)) {
            app.stopService(updaterServiceIntent);
        } else {
            app.startService(updaterServiceIntent);
        }


        // This is things related to the UI
        setContentView(R.layout.activity_main);

        ActionBar actionBar = getActionBar();

        // add the custom view to the action bar

        actionBar.setCustomView(R.layout.activity_main);

        if (preferences.getInt(MappishApplication.MAP_TYPE, MappishApplication.VIEW_PLACES) == MappishApplication.VIEW_PLACES) {
            actionBar.setSubtitle(R.string.home_button_sub_heatmap);
        } else actionBar.setSubtitle(R.string.home_button_sub_PAP);

        // Dynamically add the fragments to the action bar
        // This is for the navigation drawer
        Bundle args = savedInstanceState;
        settingsHeatmapfragment = new SettingsHeatmapFragment();
        //settingsHeatmapfragment.setArguments(args);
        settingsPathsAndPlacesFragment = new SettingsPathsAndPlacesFragment();
        //settingsPathsAndPlacesFragment.setArguments(args);
        timeDatefragment = new SettingsTimeDateFragment();
        //timeDatefragment.setArguments(args);

/*
        Fragment zMfragment = new ZoomMapFragment();
		zMfragment.setArguments(args);
		getFragmentManager().beginTransaction()
		.add(R.id.left_drawer, zMfragment).commit();
*/
        // Create the main map view
            mapFragment = new MapFragment();
            mapFragment.setArguments(savedInstanceState);

        getFragmentManager().beginTransaction()
                .add(R.id.main_content, mapFragment, "map_fragment").commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_actionbar, menu);
        this.menu = menu;
        // Initialize the menubar icon
        if (preferences.getBoolean(MappishApplication.PREF_RECORD_POS, false)) {
            menu.findItem(R.id.main_settings).setIcon(R.drawable.ic_action_location_found);
        }

        /*
        if (preferences.getInt(MappishApplication.MAP_TYPE,MappishApplication.VIEW_PLACES)==MappishApplication.VIEW_PLACES) {
            menu.findItem(R.id.paths_and_places_settings).setIcon(R.drawable.ic_action_refresh_dark);
            menu.findItem(R.id.heatmap_settings).setIcon(R.drawable.ic_action_cancel);
        } else {
            menu.findItem(R.id.paths_and_places_settings).setIcon(R.drawable.ic_action_refresh);
            menu.findItem(R.id.heatmap_settings).setIcon(R.drawable.ic_action_cancel_dark);
        }
        */

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        // Next line would start the animation on some of the sliders. I thought it looked ugly and turned it off.
        //fragmentTransaction.setCustomAnimations(R.animator.slide_in, R.animator.slide_out);

        // Switch case for which map fragment to display.
        // Each of the fragmens checks if it is the current one shwoing.
        switch (item.getItemId()) {

            case R.id.main_settings:
                if (!preferences.getBoolean(MappishApplication.PREF_RECORD_POS, true)) {
                    menu.findItem(R.id.main_settings).setIcon(R.drawable.ic_action_location_found);
                    Toast.makeText(getApplicationContext(), "Now recording your positions. Wait a few minutes for the data to show.",Toast.LENGTH_LONG).show();
                    ((MappishApplication) getApplication()).startUpdates();

                } else {
                    menu.findItem(R.id.main_settings).setIcon(R.drawable.ic_action_location_off);
                    Toast.makeText(getApplicationContext(), "Recording of positions off.",Toast.LENGTH_LONG).show();
                    ((MappishApplication) getApplication()).stopUpdates();

                }

                break;

            case R.id.time_settings:
                if (getFragmentManager().findFragmentById(R.id.sub) == (timeDatefragment)) {
                    getFragmentManager().beginTransaction().remove(timeDatefragment).commit();
                } else {
                    getFragmentManager().beginTransaction()
                            .replace(R.id.sub, timeDatefragment, FRAG_TAG).commit();
                }
                break;

            case R.id.heatmap_settings:
                getActionBar().setSubtitle(R.string.home_button_sub_heatmap);
                preferences.edit().putInt(MappishApplication.MAP_TYPE, MappishApplication.VIEW_PLACES).apply();
                if (getFragmentManager().findFragmentById(R.id.sub) == (settingsHeatmapfragment)) {
                    getFragmentManager().beginTransaction().remove(settingsHeatmapfragment).commit();
                } else {
                    getFragmentManager().beginTransaction()
                            .replace(R.id.sub, settingsHeatmapfragment, FRAG_TAG).commit();
                }
                this.controlChanged();
                break;

            case R.id.paths_and_places_settings:
                getActionBar().setSubtitle(R.string.home_button_sub_PAP);
                preferences.edit().putInt(MappishApplication.MAP_TYPE, MappishApplication.VIEW_PATHS_AND_PLACES).apply();
                if (getFragmentManager().findFragmentById(R.id.sub) == (settingsPathsAndPlacesFragment)) {
                    fragmentTransaction.remove(settingsPathsAndPlacesFragment);
                    fragmentTransaction.commit();
                } else {
                    this.controlChanged();
                    fragmentTransaction.replace(R.id.sub, settingsPathsAndPlacesFragment, FRAG_TAG);
                    fragmentTransaction.commit();
                }

                this.controlChanged();
                break;
            case R.id.show_about:
                Intent intent = new Intent(this, AboutActivity.class);
                startActivity(intent);
                break;


            case android.R.id.home:
                //Log.d(TAG, "Home Button pressed");
                Fragment f = getFragmentManager().findFragmentByTag(FRAG_TAG);
                if (f != null) {
                    getFragmentManager().beginTransaction().remove(f).commit();
                }
                break;

            default:
                break;
        }
        return true;
    }

    /*
     *  Callback from the any of the mapViews when they have changed.(non-Javadoc)
     * @see se.einarsundgren.mappish.fragment.OnMapControlChange#controllChanged()
     *
     * Its main function is to trigger a redraw of the currently used map fragment.
     */

    @Override
    public void controlChanged() {
        //
        //Invalidates and redraws the mapview.

        //mapFragment = (MapFragment) mapFragment.getFragmentManager().findFragmentById(R.id.main_content);
        mapView = mapFragment.getMapView();
        if (mapView != null) {
            //Log.d(TAG, "MapView redraws");
            int zoomLevel = preferences.getInt(MappishApplication.ZOOM_LEVEL, 15);
            MapController mc = (MapController) mapView.getController();
            mc.setZoom(zoomLevel);
            mapView.invalidate();
        } else {
            //Log.d(TAG, "Mapview was null. No redraw.");
        }
    }

    @Override
    public void onBackPressed() {
        // Overriding back button to close any control fragment if active. If they are not active,finish app.
        Fragment f = getFragmentManager().findFragmentByTag(FRAG_TAG);
        if (f != null) {
            getFragmentManager().beginTransaction().remove(f).commit();
        } else {
            finish();
        }
    }

    /*
     * Centers the map on the current map.
     */
    public void centerMap(View view) {
        //Log.d(TAG, "Centering map");
        MappishDB db = new MappishDB(this);
        MappishPoint mp = db.getLastSaved();
        if (mp != null) {
            mapFragment.getMapView().getController().setCenter(mp);
            db.close();

            //Display when the location was recorded
            Date df = new Date(((long) mp.getTime()) * 1000);
            String vv = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(df);
            Toast.makeText(getApplicationContext(), "Centered on location recorded at " + vv, Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getApplicationContext(), "No location data stored yet.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onPause(){
        super.onPause();
        //Log.d(TAG, "Main paused");
    }


    @Override
    public void onStop(){
        super.onStop();
        //Log.d(TAG, "Main " +this.getTaskId() + "stopped");
    }



    @Override
    public void onDestroy(){
        super.onDestroy();
        //getApplication().getMainLooper().quit();
        //Log.d(TAG, "Main Destroyed");

    }

    /*
     * Due to weird behaviour from the mapview on orientation changes (it looses the connection to the rest of the controls),
     * the onSaveInstanceState must be overridden and super.onSaveInstanceState can not be called with the default Bundle.
     * Do not remove unless you know what you are doing!
     */
    @Override
    public void onSaveInstanceState(Bundle bundle){
        //super.onSaveInstanceState(bundle);

        /*
        for (String key: bundle.keySet())
        {
            Log.d (TAG, key + " is a key in the bundle");
            //Log.d(TAG, "Contains:" + bundle.getFragment(key));
        }
        */
    }

}


/*
 * Copyright 2015 Einar Sundgren
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package se.einarsundgren.mappish;

import se.einarsundgren.gis.GISHelper;

public class MatrixHelper {
	int dNS;
	int dEW;
	int gridSize;
	final static String TAG = "MatrixHelper";
	
	public int getdNS() {
		return dNS;
	}

	public void setdNS(int dNS) {
		this.dNS = dNS;
	}

	public int getdEW() {
		return dEW;
	}

	public void setdEW(int dEW) {
		this.dEW = dEW;
	}

	public static int averageOfRadius(int x, int y, MappishPoint[][] matrix) {
		int acc = 0;
		@SuppressWarnings("unused")
		int divisor;

		// Standard case used most of the time
		if ((x - 1 > -1) && (y - 1 > -1) && (x + 1 < matrix.length)
				&& (y + 1 < matrix[0].length)) {
			acc += matrix[x - 1][y].strength;
			acc += matrix[x + 1][y].strength;
			acc += matrix[x][y + 1].strength;
			acc += matrix[x][y - 1].strength;
			acc += matrix[x + 1][y - 1].strength;
			acc += matrix[x + 1][y + 1].strength;
			acc += matrix[x - 1][y - 1].strength;
			acc += matrix[x - 1][y + 1].strength;
			acc += matrix[x][y].strength;
			divisor = 9;
		} else { // We are in some kind of border case.
			// Upper left corner
			if ((x - 1 < 0) && (y - 1 < 0)) {
				acc += matrix[x + 1][y].strength;
				acc += matrix[x + 1][y + 1].strength;
				acc += matrix[x][y + 1].strength;
				acc += matrix[x][y].strength;
				divisor = 4;
			} else
			// Lower left corner
			if ((x - 1 < 0) && (y + 1 > matrix[0].length)) {
				acc += matrix[x + 1][y - 1].strength;
				acc += matrix[x + 1][y + 1].strength;
				acc += matrix[x][y - 1].strength;
				acc += matrix[x][y].strength;
				divisor = 4;
			} else
			// Upper right corner
			if ((x + 1 > matrix.length) && (y - 1 < 0)) {
				acc += matrix[x - 1][y].strength;
				acc += matrix[x - 1][y + 1].strength;
				acc += matrix[x][y + 1].strength;
				acc += matrix[x][y].strength;
				divisor = 4;
			} else
			// Lower right corner
			if ((x + 1 > matrix.length) && (y + 1 > matrix[0].length)) {
				acc += matrix[x - 1][y - 1].strength;
				acc += matrix[x - 1][y].strength;
				acc += matrix[x][y - 1].strength;
				acc += matrix[x][y].strength;
				divisor = 4;
			} else
			// Towards right side
			if (x + 1 > matrix.length) {
				acc += matrix[x - 1][y - 1].strength;
				acc += matrix[x - 1][y].strength;
				acc += matrix[x][y - 1].strength;
				acc += matrix[x - 1][y].strength;
				acc += matrix[x - 1][y + 1].strength;
				acc += matrix[x][y].strength;
				divisor = 6;
			} else
			// Towards bottom
			if (y + 1 > matrix[0].length) {
				acc += matrix[x - 1][y - 1].strength;
				acc += matrix[x - 1][y].strength;
				acc += matrix[x][y - 1].strength;
				acc += matrix[x - 1][y].strength;
				acc += matrix[x + 1][y].strength;
				acc += matrix[x][y].strength;
				divisor = 6;
			} else if (x - 1 < 0) {
				acc += matrix[x][y - 1].strength;
				// acc += matrix[x][y+1].strength;
				// acc += matrix[x+1][y+1].strength;
				acc += matrix[x + 1][y - 1].strength;
				acc += matrix[x + 1][y].strength;
				acc += matrix[x][y].strength;
				divisor = 6;
			} else if (y - 1 < 0) {
				// acc += matrix[x][y-1].strength;
				acc += matrix[x][y + 1].strength;
				// acc += matrix[x+1][y+1].strength;
				// acc += matrix[x+1][y-1].strength;
				// acc += matrix[x+1][y].strength;
				acc += matrix[x][y].strength;
				divisor = 6;
			}

		}
		return acc / 8;
	}
	
	public static MappishPoint[][] gaussianBlur(MappishPoint[][] matrix) {
		MappishPoint[][] virtualMatrix = new MappishPoint[matrix.length][matrix[0].length];
		
		//Gaussian kernel r =  2. Hardcoded for speed.
		double[][] kernel = new double[][]{ 
				{0.000,0.002,0.007,0.002,0.000},
				{0.002,0.063,0.194,0.063,0.002},
				{0.007,0.194,0.598,0.194,0.007},
				{0.002,0.063,0.194,0.063,0.002},
				{0.000,0.002,0.007,0.002,0.000}};	
		
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[0].length; j++) {
				virtualMatrix[i][j] = new MappishPoint(0, 0, 0, 0);
			}
		}

		// Apply the kernel to the matrix
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[0].length; j++) {
				//if((
						virtualMatrix[i][j].strength =  Math.round(applyConvolution2D(i, j, matrix, kernel));
								//)>0)
				//Log.d(TAG, "New matrix strangth = " + virtualMatrix[i][j].strength);
			}
		}

		return virtualMatrix;
	}

	
	public static int applyConvolution2D(int x, int y,
		MappishPoint[][] matrix, double[][] kernel) {
		double acc = 0;
		int xOffs;
		int yOffs;
		//double kernelSize = (kernel.length * kernel[0].length);
		//System.out.println("Kernel Size: " +kernelSize);

		
		if ((kernel.length % 2 == 0) || (kernel[0].length == 0)) {
			// System.out.println("Kernel Error. wrong width");

		}

		xOffs = x - (kernel.length - 1) / 2;
		yOffs = y - (kernel[0].length - 1) / 2;

		for (int i = 0; i < kernel.length; i++) {
			for (int j = 0; j < kernel[0].length; j++) {
				// Check if entire kernel fits inside of matrix with kernel
				// center at x,y
				acc += getElementToRepeat(xOffs + i, yOffs + j, matrix) * kernel[i][j];
			}
		}
		
		return (int) acc;//(int) (acc / kernelSize);
	}
	
	
	public static int getElementToRepeat(int x, int y,
			MappishPoint[][] matrix) {
		if (x < 0)
			x = 0;
		
		if (x >= matrix.length)
			x = matrix.length-1;
		
		if (y < 0)
			y = 0;
		
		if (y >= matrix[0].length)
			y = matrix[0].length-1;
		//System.out.println("getting : " +x+" and "+ y);
		return matrix[x][y].strength;
	}
	
	
	public static MappishPoint[][] boxBlur(MappishPoint[][] matrix) {
		// O(n²+n²+n²) Any circumstance
		MappishPoint[][] virtualMatrix = new MappishPoint[matrix.length][matrix[0].length];

		// Create the buffer matrix and initialize.
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[0].length; j++) {
				virtualMatrix[i][j] = new MappishPoint(0, 0, 0, 0);
			}
		}

		// Calculate nearest neighbour and store i virtualMatrix
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[0].length; j++) {
				virtualMatrix[i][j] = new MappishPoint(0, 0, 0,
						matrix[i][j].strength
								+ MatrixHelper.averageOfRadius(i, j, matrix));

			}
		}

		/*
		 * // Add the matrices together. for(int i=0;i<matrix.length;i++){
		 * for(int j = 0; j<matrix[0].length; j++){
		 * matrix[i][j].strength+=virtualMatrix[i][j].strength; } } return
		 * matrix;
		 */
		return virtualMatrix;
	}

	
	// Non static version
	public MappishPoint[][] createMatrix(int eastToWestMeters, int northToSouthMeters, int gridSizeMeters){
		this.dNS = northToSouthMeters/gridSizeMeters; //
		this.gridSize = gridSizeMeters;
		//System.out.println("NS elements" + northToSouthMeters/gridSizeMeters);
		//System.out.println("EW elements" + eastToWestMeters/gridSizeMeters);
		this.dEW = eastToWestMeters/gridSizeMeters; //

		//Log.d(TAG, "Generated matrix at the size " + ( ((eastToWestMeters/gridSizeMeters)+3) + " by " + ((northToSouthMeters/gridSizeMeters)+3)));
		return new MappishPoint[(this.dEW)+3][(this.dNS)+3];
	}
	
	
	public static MappishPoint[][] createMatrix(int eastToWestMeters, int northToSouthMeters, int gridSizeMeters, boolean stat){
		int dNS = northToSouthMeters/gridSizeMeters; //
		int dEW = eastToWestMeters/gridSizeMeters; //
		return new MappishPoint[(dEW)+3][(dNS)+3];
	}
	
	
	
	
	// Takes a point from the database. Calculates its x,y offset in meters 
	// to the lat/lon of upper left corner (box.LatNorth/box.LonWest) 
	// And multiplies that by 100 since the value is given in km and we 
	// create a matrix representing the map divided in a 10 m grid.
	// We assume the last run of createMatrix is the matrix to map to.
	//TODO: Remove the magic numbers
	public int[] mapToMatrix(double startLat, double startLon, double endLat, double endLon){
	int yOffsetMeters = (int) Math.round((GISHelper.haversin(
			startLat, 
			startLon, 
			endLat,
			startLon)*1000)/this.gridSize);
	
	int xOffsetMeters = (int) Math.round((GISHelper.haversin(
			startLat, 
			startLon, 
			startLat, 
			endLon)*1000/this.gridSize));
	
	return new int [] {xOffsetMeters,yOffsetMeters};
	}
	
	/**
	 * Static version of the previous
	 */

	public static int[] mapToMatrix(double startLat, double startLon, double endLat, double endLon, int gridSize){
	int yOffsetMeters = (int) Math.round((GISHelper.haversin(
			startLat, 
			startLon, 
			endLat,
			startLon)*1000)/gridSize);
	
	int xOffsetMeters = (int) Math.round((GISHelper.haversin(
			startLat, 
			startLon, 
			startLat, 
			endLon)*1000/gridSize));
	
	return new int [] {xOffsetMeters, yOffsetMeters};
	}
	

}

/* Debug kernels. Not Gaussian
double[][] kernel = new double[][] { 
		{ 2.0, 2.0, 2.0, 2.0, 2.0 },
		{ 2.0, 5.0, 5.0, 5.0, 2.0 }, 
		{ 2.0, 5.0, 10.0,5.0, 2.0 },
		{ 2.0, 5.0, 5.0, 5.0, 2.0 }, 
		{ 2.0, 2.0, 2.0, 2.0, 2.0 } };

double[][] kernel = new double[][] { 
		{ 0.2, 0.2, 0.2, 0.2, 0.2 },
		{ 0.2, 0.5, 0.5, 0.5, 0.2 }, 
		{ 0.2, 0.5, 1.0, 0.5, 0.2 },
		{ 0.2, 0.5, 0.5, 0.5, 0.2}, 
		{ 0.2, 0.2, 0.2, 0.2, 0.2 } };
*/

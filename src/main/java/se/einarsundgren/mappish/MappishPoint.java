
/*
 * Copyright 2015 Einar Sundgren
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package se.einarsundgren.mappish;

import org.osmdroid.util.GeoPoint;

/**
 * Extending GeoPoint to make it possible to also supply a date in the same class.
 */

public class MappishPoint extends GeoPoint{

	private static final long serialVersionUID = 1L;
	private int time;
    private boolean isPlace;
    public int strength; // Set to default to make the matrix manipulations more straightforward
	public MappishPoint(double aLatitude, double aLongitude, int time ) {
		super(aLatitude, aLongitude);
		this.setTime(time);
		strength = 1;
	}
	
	public MappishPoint(double aLatitude, double aLongitude, int time, int strength) {
		super(aLatitude, aLongitude);
		this.setTime(time);
		this.strength=strength;
	}
	
	// Copy constructor
	public MappishPoint(MappishPoint mappishPoint) {
		super(mappishPoint);
		this.setTime(mappishPoint.getTime());
		this.strength = mappishPoint.getStrength();
	}

    public void setIsPlace(boolean isPlace){
        this.isPlace = isPlace;
    }

    public boolean isPlace(){
        return isPlace;
    }

	public int getTime() {
		return time;
	}
	public void setTime(int time) {
		this.time = time;
	}

	public int getStrength() {
		return strength;
	}

	public void setStrength(int strength) {
		this.strength = strength;
	}

}

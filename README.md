# README #

Mappish, or as It soon might be called "Paths and Places". Is an app that visualizes your geolocation data over time. To put it simpler, it saves the positions of your mobile phone and let you show on a map where you have spent your day.

### What is this repository for? ###

* This is a development repository for a one man team. There is no guarantee that the current state of the code will compile though my intention is to allways push in a working state.
* Version 0.9

### How do I get set up? ###

* Setup is no different than for any other android project.
* The project uses Open street map and in particular the library OSMdroid. https://github.com/osmdroid/osmdroid 
* There is a nice preference setting that allows you to stop saving the geolocation data. That is good for privacy.
* There are no regression testing done yet. That is obvioulsy a flaw tha twill be fixed.

### Contribution guidelines ###

* Since this is a one man project so far the guidlines are kept private.

### Who do I talk to? ###

* If you want to contribute in any way, please contact me Einar Sundgren, that is the repo owner.